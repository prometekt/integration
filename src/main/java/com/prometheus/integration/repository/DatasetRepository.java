package com.prometheus.integration.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class DatasetRepository {

    @Autowired
    EntityManager entityManager;

    public List<String> getDatasets(){
        List<String> datasets = entityManager.createNativeQuery("SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'").getResultList();
        return datasets;
    }

    public List<String[]> getColumns(String dataset){
        List<String[]> columns = (List<String[]>)(entityManager.createNativeQuery("SELECT column_name, data_type  FROM information_schema.columns WHERE table_schema='public' AND table_name='"+dataset+"'").getResultList());
        return columns;
    }


}



