package com.prometheus.integration.repository;

import com.prometheus.integration.entity.datasource.DBDatasource;
import com.prometheus.integration.entity.datasource.Datasource;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatasourceRepository extends MongoRepository<DBDatasource, String> {



}
