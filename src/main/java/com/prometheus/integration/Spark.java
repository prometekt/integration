package com.prometheus.integration;


import org.apache.spark.sql.SparkSession;

public class Spark {

    public static SparkSession spark = SparkSession
            .builder()
            .appName("Java Spark SQL basic example")
            .config("spark.master", "local")
            .getOrCreate();

}


