package com.prometheus.integration.entity.datasource;

import com.prometheus.integration.Spark;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.sql.*;

@Document(collection = "datasources")
public class DBDatasource {

    @Id
    public String id;

    @Field
    private String name;


    @Field
    private String driver;

    @Field
    private String host;

    @Field
    private Integer port;

    @Field
    private String database;

    @Field
    private String query;

    @Field
    private String username;

    @Field
    private String password;


    public DBDatasource(String name, String driver, String host, Integer port, String database, String query, String username, String password) {
        this.name = name;
        this.driver = driver;
        this.host = host;
        this.port = port;
        this.database = database;
        this.query = query;
        this.username = username;
        this.password = password;
    }

    /*
    public Dataset<Row> loadData() {

        String query = "("+this.query+") data";
        Dataset<Row> dataset = Spark.spark.read()
                .format("jdbc")
                .option("url", driver + "://" + host + ":" + port + "/" + database)
                .option("dbtable", query)
                .option("user", user)
                .option("password", password)
                .load();

        return dataset;
    } */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String user) {
        this.username = user;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}
