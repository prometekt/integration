package com.prometheus.integration.entity.datasource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prometheus.integration.Spark;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


@Document(collection = "datasources")
public class RestAPIDatasource extends Datasource {

    @Field
    public static final String type = "restapi";


    @Field
    private String url;

    @Field
    private String apiKey;

    public Dataset<Row> loadData() {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String response = restTemplate.exchange("https://jsonplaceholder.typicode.com/todos/1", HttpMethod.GET, entity, String.class).getBody();

        try (PrintWriter out = new PrintWriter("loaded.json")) {
            out.println(response);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Dataset<Row> dataset = Spark.spark.read().option("multiline", true).format("json").load("loaded.json");

        return dataset;
    }

    public static String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
