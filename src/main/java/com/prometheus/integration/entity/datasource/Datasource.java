package com.prometheus.integration.entity.datasource;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;


/*@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DBDatasource.class, name = "database"),
        @JsonSubTypes.Type(value = RestAPIDatasource.class, name = "restapi")
}) */
@Document(collection = "datasources")
public class Datasource {

    @Id
    private String id;

    @Field
    private String name;

    public Dataset<Row> loadData() {

        return null;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
