package com.prometheus.integration.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.prometheus.integration.Spark;
import com.prometheus.integration.repository.DatasetRepository;
import org.apache.spark.sql.functions;
import org.apache.commons.io.IOUtils;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


@RestController
@RequestMapping("/api/datasets")
@CrossOrigin(origins = "*")
public class DatasetController {


    @Autowired
    DatasetRepository datasetRepository;


    @GetMapping("")
    @CrossOrigin(origins = "*")
    public ArrayNode datasets() {

        ObjectMapper mapper = new ObjectMapper();

        ArrayNode arrayNode = mapper.createArrayNode();

        for (String dataset : datasetRepository.getDatasets()) {
            List<String[]> columns = datasetRepository.getColumns(dataset);
            ObjectNode childNode = mapper.createObjectNode();
            childNode.put("name", dataset);

            ObjectNode colNode = mapper.createObjectNode();
            for (Object[] colData : columns) {
                colNode.put(colData[0].toString(), colData[1].toString());
            }

            childNode.put("columns", colNode);
            arrayNode.add(childNode);
        }

        return arrayNode;
    }

    @PostMapping("/upload")
    @CrossOrigin(origins = "*")
    public String uploadDataset(@RequestParam("file") MultipartFile file) {

        File targetFile = saveFile(file);

        Dataset<Row> dataset = Spark.spark.read().format("csv")
                .option("sep", ",")
                .option("inferSchema", "true")
                .option("header", "true")
                .load(file.getOriginalFilename());

        dataset = dataset.withColumn("index", functions.monotonically_increasing_id());

        dataset.write()
                .format("jdbc")
                .option("url", "jdbc:postgresql://localhost/datasets")
                .option("dbtable", file.getOriginalFilename().replace(".csv", ""))
                .option("user", "postgres")
                .option("password", "parool")
                .save();

        targetFile.delete();

        return "Uploaded";
    }


    public File saveFile(MultipartFile file) {
        InputStream initialStream = null;
        File targetFile = null;
        try {
            initialStream = file.getInputStream();
            targetFile = new File(file.getOriginalFilename());
            OutputStream outStream = new FileOutputStream(targetFile);

            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = initialStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            //IOUtils.closeQuietly(initialStream);
            //IOUtils.closeQuietly(outStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return targetFile;

    }

    @GetMapping("/{name}")
    public JsonNode getDataset(@PathVariable String name) {

        JsonNode actualObj = null;


        Dataset<Row> kyphosis = Spark.spark.read()
                .format("jdbc")
                .option("url", "jdbc:postgresql://localhost/datasets")
                .option("dbtable", name)
                .option("user", "postgres")
                .option("password", "parool")
                .load();

        ObjectMapper mapper = new ObjectMapper();

        List<String[]> columns = datasetRepository.getColumns(name);
        ObjectNode childNode = mapper.createObjectNode();
        childNode.put("name", name);

        ObjectNode colNode = mapper.createObjectNode();
        for (Object[] colData : columns) {
            colNode.put(colData[0].toString(), colData[1].toString());
        }

        childNode.put("columns", colNode);

        try {
            actualObj = mapper.readTree(kyphosis.toJSON().collectAsList().toString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        childNode.put("data", actualObj);

        return childNode;
    }

    @GetMapping("/regression")
    public String regression(@RequestParam String name, @RequestParam String featureColumn, @RequestParam String targetColumn) {

        Dataset<Row> training = Spark.spark.read()
                .format("jdbc")
                .option("url", "jdbc:postgresql://localhost/datasets")
                .option("dbtable", name)
                .option("user", "postgres")
                .option("password", "parool")
                .load();

        LinearRegression lr = new LinearRegression()
                .setMaxIter(10)
                .setRegParam(0.3)
                .setElasticNetParam(0.8);

        VectorAssembler vector = new VectorAssembler()
                .setInputCols(new String[]{"Start", "Age"})
                .setOutputCol("features");

        lr.setFeaturesCol("features");
        

        LinearRegressionModel lrModel = lr.fit(training);


        System.out.println("Coefficients: "
                + lrModel.coefficients() + " Intercept: " + lrModel.intercept());

        LinearRegressionTrainingSummary trainingSummary = lrModel.summary();
        System.out.println("numIterations: " + trainingSummary.totalIterations());
        System.out.println("objectiveHistory: " + Vectors.dense(trainingSummary.objectiveHistory()));
        trainingSummary.residuals().show();
        System.out.println("RMSE: " + trainingSummary.rootMeanSquaredError());
        System.out.println("r2: " + trainingSummary.r2());

        return "Result";
    }


}
