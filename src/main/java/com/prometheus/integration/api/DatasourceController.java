package com.prometheus.integration.api;

//import com.prometheus.integration.repository.DBDatasourceRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.prometheus.integration.Spark;
import com.prometheus.integration.entity.datasource.DBDatasource;
import com.prometheus.integration.entity.datasource.Datasource;
import com.prometheus.integration.entity.datasource.RestAPIDatasource;
import com.prometheus.integration.repository.DatasetRepository;
import com.prometheus.integration.repository.DatasourceRepository;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/datasources")
@CrossOrigin(origins = "*")
public class DatasourceController {

    @Autowired
    DatasourceRepository datasourceRepository;

    @Autowired
    DatasetRepository datasetRepository;

    @GetMapping
    public List<DBDatasource> getDatasources() {
        List<DBDatasource> datasources = new ArrayList<>();
        Iterable<DBDatasource> result = datasourceRepository.findAll();
        for (DBDatasource datasource : result) {
            datasources.add(datasource);
        }
        return datasources;
    }

    @PostMapping
    public DBDatasource addDBDatasource(@RequestBody DBDatasource datasource) {
        datasourceRepository.save(datasource);

        testConnection(datasource);
        return datasource;
    }

    @DeleteMapping("/{name}")
    public DBDatasource deleteDatasource(@PathVariable String name) {
        Iterable<DBDatasource> result = datasourceRepository.findAll();

        for (DBDatasource datasource : result) {
            if (datasource.getName().equals(name)) {
                datasourceRepository.delete(datasource);
                return datasource;
            }
        }

        return null;
    }

    private void testConnection(DBDatasource datasource) {
        String url = "jdbc:postgresql://" + datasource.getHost() + ":" + datasource.getPort() + "/" + datasource.getDatabase();
        Properties props = new Properties();
        props.setProperty("user", datasource.getUsername());
        props.setProperty("password", datasource.getPassword());
        try {
            Connection conn = DriverManager.getConnection(url, props);
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(datasource.getQuery());

            while(rs.next()) {
                System.out.println();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @GetMapping("/query")
    public JsonNode queryData(@RequestParam String sql) {

        String url = "jdbc:postgresql://" + "localhost" + ":" + "5432" + "/datasets";
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", "parool");

        Connection conn = null;
        ResultSet rs = null;
        Statement statement = null;

        ObjectMapper mapper = new ObjectMapper();

        ObjectNode responseNode = mapper.createObjectNode();
        ObjectNode colNode = mapper.createObjectNode();
        ArrayNode dataNode = mapper.createArrayNode();

        try {
            conn = DriverManager.getConnection(url, props);
            statement = conn.createStatement();
            rs = statement.executeQuery(sql);

            responseNode.put("name", rs.getMetaData().getTableName(1));

            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                colNode.put(rs.getMetaData().getColumnName(i), rs.getMetaData().getColumnType(i));
            }

            responseNode.put("columns", colNode);

            int index = 1;
            while(rs.next()) {
                ObjectNode rowNode = mapper.createObjectNode();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    rowNode.put(rs.getMetaData().getColumnName(i), rs.getString(i));
                }
                rowNode.put("index", index++);
                dataNode.add(rowNode);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                rs.close();
                statement.close();
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        responseNode.put("data", dataNode);

        return responseNode;
    }

    @GetMapping("/{name}/import")
    public String importDataIntoWarehouse(@PathVariable String name) {

        String sql = "";
        DBDatasource foundDatasource = null;
        Iterable<DBDatasource> result = datasourceRepository.findAll();
        for (DBDatasource datasource : result) {
            if (datasource.getName().equals(name)) {
                sql = datasource.getQuery();
                foundDatasource = datasource;
                break;
            }
        }

        String url = "jdbc:postgresql://" + "localhost" + ":" + "5432" + "/" + foundDatasource.getDatabase();
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", "parool");

        Connection source = null;
        Connection target = null;
        ResultSet rs = null;
        Statement statement = null;

        try {
            source = DriverManager.getConnection(url, props);
            target = DriverManager.getConnection("jdbc:postgresql://" + "localhost" + ":" + "5432" + "/datasets", props);
            statement = source.createStatement();
            rs = statement.executeQuery(sql);

            ResultSetMetaData meta = rs.getMetaData();

            List<String> columns = new ArrayList<>();
            for (int i = 1; i <= meta.getColumnCount(); i++)
                columns.add(meta.getColumnName(i));

            try (PreparedStatement s2 = target.prepareStatement(
                    "INSERT INTO " + foundDatasource.getName() + " ("
                            + columns.stream().collect(Collectors.joining(", "))
                            + ") VALUES ("
                            + columns.stream().map(c -> "?").collect(Collectors.joining(", "))
                            + ")"
            )) {

                while (rs.next()) {
                    for (int i = 1; i <= meta.getColumnCount(); i++)
                        s2.setObject(i, rs.getObject(i));

                    s2.addBatch();
                }

                s2.executeBatch();
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                rs.close();
                statement.close();
                source.close();
                target.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return "Imported";
    }

    @GetMapping("/{name}/load")
    public JsonNode loadDataFromDatasource(@PathVariable String name) {

        String sql = "";
        DBDatasource foundDatasource = null;
        Iterable<DBDatasource> result = datasourceRepository.findAll();
        for (DBDatasource datasource : result) {
            if (datasource.getName().equals(name)) {
                sql = datasource.getQuery();
                foundDatasource = datasource;
                break;
            }
        }

        String url = "jdbc:postgresql://" + "localhost" + ":" + "5432" + "/" + foundDatasource.getDatabase();
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", "parool");

        Connection conn = null;
        ResultSet rs = null;
        Statement statement = null;

        ObjectMapper mapper = new ObjectMapper();

        ObjectNode responseNode = mapper.createObjectNode();
        ObjectNode colNode = mapper.createObjectNode();
        ArrayNode dataNode = mapper.createArrayNode();

        try {
            conn = DriverManager.getConnection(url, props);
            statement = conn.createStatement();
            rs = statement.executeQuery(sql);

            responseNode.put("name", rs.getMetaData().getTableName(1));

            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                colNode.put(rs.getMetaData().getColumnName(i), rs.getMetaData().getColumnType(i));
            }

            responseNode.put("columns", colNode);

            int index = 1;
            while(rs.next()) {
                ObjectNode rowNode = mapper.createObjectNode();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    rowNode.put(rs.getMetaData().getColumnName(i), rs.getString(i));
                }
                rowNode.put("index", index++);
                dataNode.add(rowNode);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                rs.close();
                statement.close();
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        responseNode.put("data", dataNode);

        return responseNode;
    }


    @GetMapping("/load")
    public JsonNode loadData() {

        JsonNode actualObj = null;

        /*
        String query = "SELECT * FROM customers";
        DBDatasource datasource = new DBDatasource("jdbc:mysql", "localhost", 3306, "Northwind", query, "root", "parool") ;
        datasource.setDatabase("Northwind");
        Dataset<Row> data = datasource.loadData();

        data.write()
                .format("jdbc")
                .option("url", "jdbc:postgresql:database")
                .option("dbtable", "customers")
                .option("user", "reemet")
                .option("password", "parool")
                .save();




        ObjectMapper mapper = new ObjectMapper();
        try {
            actualObj = mapper.readTree(data.toJSON().collectAsList().toString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } */

        RestAPIDatasource apiDatasource = new RestAPIDatasource();
        Dataset<Row> data = apiDatasource.loadData();

        ObjectMapper mapper = new ObjectMapper();
        try {
            actualObj = mapper.readTree(data.toJSON().collectAsList().toString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return actualObj;
    }




}
