package com.prometheus;

import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Configuration
public class BeanConfig {

    @PersistenceUnit
    EntityManagerFactory emf;

}