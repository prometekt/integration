FROM adoptopenjdk/openjdk9
COPY build/libs/integration-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]